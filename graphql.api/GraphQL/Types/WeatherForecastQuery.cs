using System;
using Graphql.API.Models;
using GraphQL;
using GraphQL.Types;
using Graphql.API.Application.WeatherForecast.Graph;

namespace Graphql.API.GraphQL.Types
{
    public class WeatherForecastQuery : ObjectGraphType
    {
        private readonly IWeatherForecastManager _weatherForecastManager;

        public WeatherForecastQuery(IWeatherForecastManager weatherForecastManager)
        {
            _weatherForecastManager = weatherForecastManager;

            AddWeatherForecastQuery();
            AddWeatherForecastQueryById();
        }

        private void AddWeatherForecastQuery()
        {
            Field<ListGraphType<WeatherForecastType>>(
                name: "allwfs",
                resolve: context => _weatherForecastManager.GetAll());
        }

        private void AddWeatherForecastQueryById()
        {
            Field<ListGraphType<WeatherForecastType>>(
                name: "wfswithcommand",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>>
                {
                    Name = "command"
                }),
                resolve: context => _weatherForecastManager.GetByCommand(context.GetArgument<byte>("command"))
            );
        }
    }
}
