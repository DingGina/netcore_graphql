using System;
using Graphql.API.Models;
using GraphQL.Types;
using Graphql.API.Application.WeatherForecast;

namespace Graphql.API.GraphQL.Types
{
    public class WeatherForecastType : ObjectGraphType<WeatherForecastModel>
    {
        public WeatherForecastType(ICommandCache commandCache)
        {
            Field(t => t.Date, type: typeof(DateTimeGraphType));
            Field(t => t.TemperatureC);
            Field(t => t.TemperatureF);
            Field(t => t.Summary);
            Field(t => t.CommandSequence, type: typeof(IdGraphType));
            Field(name: "Command",
                type: typeof(CommandType),
                resolve: context => commandCache.GetCommand(context.Source.CommandSequence)
            );
        }
    }
}
